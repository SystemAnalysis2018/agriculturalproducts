'use strict'
import util from './util'

const ApiUrl = 'Articles'

export default {
  /**
   * 返回所有栏目
   */
  getAll () {
    return util.get(ApiUrl)
  },
  /**
   * 新建栏目
   * @param {*} param0
   */
  create ({
    title,
    content,
    columnId
  }) {
    return util.postString(ApiUrl, {
      title,
      content,
      columnId
    })
  }

}
