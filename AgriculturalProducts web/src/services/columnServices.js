'use strict'
import util from './util'

const ApiUrl = 'Columns'

export default {
  /**
   * 返回所有栏目
   */
  getAll () {
    return util.get(ApiUrl)
  },
  /**
   * 新建栏目
   * @param {*} param0
   */
  create ({
    columnName,
    isSingle
  }) {
    return util.postString(ApiUrl, {
      columnName,
      isSingle
    })
  },
  IsColumnUsed (columnName) {
    return util.get(ApiUrl + '/IsColumnUsed/' + columnName)
  },
  /**
   * 删除栏目
   * @param {} id
   */
  delete (id) {
    return util.delete(ApiUrl + '/' + id)
  },
  /**
   * 栏目名称是否被其它栏目使用
   */
  IsColumnNameUsedOtherId (columnName, id) {
    return util.get(ApiUrl + '/IsColumnUsed/' + columnName + '/' + id)
  },
  edit (currentrow) {
    return util.putString(ApiUrl + '/' + currentrow.id, currentrow)
  }

}
