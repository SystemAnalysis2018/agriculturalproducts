'use strict'
import util from './util'

const ApiUrl = 'ColumnViewModels'

export default {

  getAll () {
    return util.get(ApiUrl)
  }

}
