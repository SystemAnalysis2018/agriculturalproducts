'use strict'
import util from './util'

const ApiUrl = 'ProductViewModel'

export default {

  getAll () {
    return util.get(ApiUrl)
  }

}
