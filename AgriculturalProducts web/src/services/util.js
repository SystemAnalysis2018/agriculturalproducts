'use strict'

import $ from 'jquery'
import store from '@/stores/index.js'

const API_ROOT = 'https://localhost:5001/api/'

/* eslint-disable */
export default {
  post(url, data) {
    return $.ajax({
      headers: {
        Authorization: store.state.user.token
      },
      url: API_ROOT + url,
      method: 'POST',
      data: data,
      catch: false
    })
  },
  postString(url, data) {
    return $.ajax({
      headers: {
        Authorization: store.state.user.token
      },
      url: API_ROOT + url,
      method: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      dataType: 'json',
      catch: false
    })
  },
  get(url, data) {
    return $.ajax({
      headers: {
        Authorization: store.state.user.token
      },
      url: API_ROOT + url,
      data: data,
      method: 'GET',
      cache: false
    })
  },
  put(url, data) {
    return $.ajax({
      headers: {
        Authorization: store.state.user.token
      },
      url: API_ROOT + url,
      data: data,
      method: 'put',
      cache: false
    })
  },
  putString(url, data) {
    return $.ajax({
      headers: {
        Authorization: store.state.user.token
      },
      url: API_ROOT + url,
      method: 'put',
      data: JSON.stringify(data),
      contentType: 'application/json',
      dataType: 'json',
      catch: false
    })
  },
  delete(url, data) {
    if (!data) {
      return $.ajax({
        headers: {
          Authorization: store.state.user.token
        },
        url: API_ROOT + url,
        method: 'delete',
        cache: false
      })
    }
    data._method = 'Delete'
    return $.ajax({
      headers: {
        Authorization: store.state.user.token
      },
      url: API_ROOT + url,
      data: data,
      method: 'post',
      cache: false
    })
  },

  formatDate(dateValue, fmt) {
    var o = {
      "M+": dateValue.getMonth() + 1, //月份
      "d+": dateValue.getDate(), //日
      "h+": dateValue.getHours(), //小时
      "m+": dateValue.getMinutes(), //分
      "s+": dateValue.getSeconds(), //秒
      "q+": Math.floor((dateValue.getMonth() + 3) / 3), //季度
      "S": dateValue.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt))
      fmt = fmt.replace(RegExp.$1, (dateValue.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
      if (new RegExp("(" + k + ")").test(fmt))
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
  }
}
