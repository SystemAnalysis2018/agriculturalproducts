'use strict'
import util from './util'

const ApiUrl = 'Sellers'

export default {
  /**
   *
   */
  getAll () {
    return util.get(ApiUrl)
  },
  /**
   * 新建卖家
   * @param {*} param0
   */
  create ({
    productName,
    characteristic,
    amount,
    // picture,
    price

  }) {
    return util.postString(ApiUrl, {
      productName,
      characteristic,
      amount,
      // picture,
      price
    })
  },
  /**
   * 删除卖家商品
   * @param {} id
   */
  delete (id) {
    return util.delete(ApiUrl + `/${id}`)
  }
}
