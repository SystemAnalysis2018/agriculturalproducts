'use strict'
import util from './util'

const ApiUrl = 'login'

export default {
  /**
   * 获取验证码
   */
  GetCaptcha2 () {
    return util.get(ApiUrl + '/captcha')
  },
  login (loginForm) {
    return util.postString(ApiUrl, loginForm)
  }

}
