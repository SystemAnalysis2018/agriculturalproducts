'use strict'
import util from './util'

const ApiUrl = 'Orders'

export default {
  /**
   *
   */
  getAll () {
    return util.get(ApiUrl)
  },
  /**
   * 新建订单
   * @param {*} param0
   */
  create ({
    productName,
    characteristic,
    amount,
    // picture,
    price

  }) {
    return util.postString(ApiUrl, {
      productName,
      characteristic,
      amount,
      // picture,
      price
    })
  },
  /**
   * 删除订单
   * @param {} id
   */
  delete (id) {
    return util.delete(ApiUrl + `/${id}`)
  }
}
