'use strict'
import util from './util'

const ApiUrl = 'Buyer'

export default {
  /**
   *
   */
  getAll () {
    return util.get(ApiUrl)
  },
  /**
   * @param {*} param0
   */
  create ({
    productName,
    characteristic,
    amount,
    // picture,
    price

  }) {
    return util.postString(ApiUrl, {
      productName,
      characteristic,
      amount,
      // picture,
      price
    })
  },
  /**
   * 删除买家加入的商品
   * @param {} id
   */
  delete (id) {
    return util.delete(ApiUrl + `/${id}`)
  }
}
