'use strict'
import util from './util'

const ApiUrl = 'ShoppingCartsServices'

export default {
  /**
   *
   */
  getAll () {
    return util.get(ApiUrl)
  },
  /**
   * 添加商品
   * @param {*} param0
   */
  create ({
    productName,
    characteristic,
    amount,
    // picture,
    price

  }) {
    return util.postString(ApiUrl, {
      productName,
      characteristic,
      amount,
      // picture,
      price
    })
  },
  /**
   * 删除商品
   * @param {} id
   */
  delete (id) {
    return util.delete(ApiUrl + `/${id}`)
  }
}
