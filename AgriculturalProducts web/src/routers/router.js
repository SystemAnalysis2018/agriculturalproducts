import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/mainpage',
      name: 'mainpage',
      component: () => import(/* webpackChunkName: "about" */ '@/views/MainPage.vue'),
      children: [
        {
          path: 'columncreate',
          name: 'columncreate',
          component: () => import('@/views/columns/columncreate.vue')
        },
        {
          path: 'columnlist',
          name: 'columnlist',
          component: () => import('@/views/columns/columnList.vue')
        },
        {
          path: 'articlecreate',
          name: 'articlecreate',
          component: () => import('@/views/articles/articlecreate.vue')
        },
        {
          path: 'leafyvegetables',
          name: 'leafyvegetables',
          component: () => import('@/views/articles/leafyvegetables.vue')
        }, {
          path: 'Cauliflowervegetables',
          name: 'Cauliflowervegetables',
          component: () => import('@/views/articles/Cauliflowervegetables.vue')
        }, {
          path: 'Fruitsandvegetables',
          name: 'Fruitsandvegetables',
          component: () => import('@/views/articles/Fruitsandvegetables.vue')
        }, {
          path: 'rootvegetables',
          name: 'rootvegetables',
          component: () => import('@/views/articles/rootvegetables.vue')
        }, {
          path: 'stemvegetables',
          name: 'stemvegetables',
          component: () => import('@/views/articles/stemvegetables.vue')
        }, {
          path: 'Home2',
          name: 'Home2',
          component: () => import('@/views/Home2.vue')
        }, {
          path: 'articlelist',
          name: 'articlelist',
          component: () => import('@/views/articles/articlelist.vue')
        },
        {
          path: 'factorygoods',
          name: 'factorygoods',
          component: () => import('@/views/articles/factorygoods.vue')
        },
        {
          path: 'sellinggoods',
          name: 'sellinggoods',
          component: () => import('@/views/articles/sellinggoods.vue')
        },
        {
          path: 'capitalflow',
          name: 'capitalflow',
          component: () => import('@/views/articles/capitalflow.vue')
        },
        {
          path: 'goodsedit',
          name: 'goodsedit',
          component: () => import('@/views/articles/goodsedit.vue')
        },
        {
          path: 'shoppingcart',
          name: 'shoppingcart',
          component: () => import('@/views/articles/shoppingcart.vue')
        },
        {
          path: 'myorder',
          name: 'myorder',
          component: () => import('@/views/articles/myorder.vue')
        },
        {
          path: 'tobepaid',
          name: 'tobepaid',
          component: () => import('@/views/articles/tobepaid.vue')
        },
        {
          path: 'berryfruit',
          name: 'berryfruit',
          component: () => import('@/views/columns/berryfruit.vue')
        }, {
          path: 'melonfruit',
          name: 'melonfruit',
          component: () => import('@/views/columns/melonfruit.vue')
        },{
          path: 'egg',
          name: 'egg',
          component: () => import('@/views/columns/egg.vue')
        },{
          path: 'livestock',
          name: 'livestock',
          component: () => import('@/views/columns/livestock.vue')
        },{
          path: 'poultry',
          name: 'poultry',
          component: () => import('@/views/columns/poultry.vue')
        }, {
          path: 'pomefruit',
          name: 'pomefruit',
          component: () => import('@/views/columns/pomefruit.vue')
        }
      ]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue')
    }
  ]
})
