import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/mainpage',
      name: 'mainpage',
      component: () => import(/* webpackChunkName: "about" */ '@/views/MainPage.vue'),
      children: [
        {
          path: 'columncreate',
          name: 'columncreate',
          component: () => import('@/views/columns/columncreate.vue')
        },
        {
          path: 'columnlist',
          name: 'columnlist',
          component: () => import('@/views/columns/columnList.vue')
        },
        {
          path: 'articlecreate',
          name: 'articlecreate',
          component: () => import('@/views/articles/articlecreate.vue')
        },
        {
          path: 'articlelist',
          name: 'articlelist',
          component: () => import('@/views/articles/articlelist.vue')
        },
        {
          path: 'factorygoods',
          name: 'factorygoods',
          component: () => import('@/views/columns/factorygoods.vue')
        },
        {
          path: 'sellinggoods',
          name: 'sellinggoods',
          component: () => import('@/views/columns/sellinggoods.vue')
        },
        {
          path: 'capitalflow',
          name: 'capitalflow',
          component: () => import('@/views/columns/capitalflow.vue')
        },
        {
          path: 'shoppingcart',
          name: 'shoppingcart',
          component: () => import('@/views/columns/shoppingcart.vue')
        },
        {
          path: 'tobepaid',
          name: 'tobepaid',
          component: () => import('@/views/columns/tobepaid.vue')
        }
      ]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue')
    }
  ]
})
