﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AgricultralProductWebAPI.Migrations
{
    public partial class v004 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Picture",
                table: "ShoppingCarts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "Picture",
                table: "ShoppingCarts",
                nullable: false,
                defaultValue: new byte[] {  });
        }
    }
}
