﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AgricultralProductWebAPI.Models.EF
{
    public class OrderDetail
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// 订单Id
        /// </summary>
        [Required]
        public Guid OrderId { get; set; }

        /// <summary>
        /// 产品Id
        /// </summary>
        [Required]
        public Guid ProductId { get; set; }

        /// <summary>
        /// 价格
        /// </summary>
        [Required]
        public int Price { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Required]
        public int Amount { get; set; }
    }
}
