﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AgricultralProductWebAPI.Models.EF
{
    public class Buyer
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// 买家昵称
        /// </summary>
        [Required]
        public string NickName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// 买家地址
        /// </summary>
        [Required]
        public string Address { get; set; }

        /// <summary>
        /// 买家联系方式
        /// </summary>
        [Required]
        public string Tel { get; set; }

        /// <summary>
        /// 买家注册时间
        /// </summary>
        [Required]
        public DateTime EstablishedTime { get; set; }


    }
}
