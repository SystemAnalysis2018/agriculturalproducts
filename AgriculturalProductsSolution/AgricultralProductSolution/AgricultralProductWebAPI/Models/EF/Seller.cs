﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AgricultralProductWebAPI.Models.EF
{
    public class Seller
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid Id { get; set; }
        

        /// <summary>
        /// 卖家昵称
        /// </summary>
        [Required]
        public string NickName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// 卖家本名
        /// </summary>
        [Required]
        public string RealName { get; set; }

        /// <summary>
        /// 店铺地址
        /// </summary>
        [Required]
        public string StoreAddress { get; set; }

        /// <summary>
        /// 店铺联系方式
        /// </summary>
        [Required]
        public string StoreTel { get; set; }

        /// <summary>
        /// 卖家注册时间
        /// </summary>
        [Required]
        public DateTime EstablishedTime { get; set; }


    }
}
