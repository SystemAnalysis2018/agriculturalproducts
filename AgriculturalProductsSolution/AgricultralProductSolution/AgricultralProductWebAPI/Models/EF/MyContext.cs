﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace AgricultralProductWebAPI.Models.EF
{
    public class MyContext: DbContext
    {
        /// <inheritdoc />
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="options"></param>
        public MyContext(DbContextOptions<MyContext> options) : base(options)
        {
       
        }
        /// <summary>
        /// 销售者
        /// </summary>
        public DbSet<Buyer> Buyers { get; set; }

        public DbSet<MoneyDetail> MoneyDetails { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderDetail> OrderDetails { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Seller> Sellers { get; set; }

        public DbSet<ShoppingCart> ShoppingCarts { get; set; }



    }
}
