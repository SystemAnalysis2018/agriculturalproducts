﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AgricultralProductWebAPI.Models.EF
{
    public class MoneyDetail
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// 金额变动数量
        /// </summary>
        [Required]
        public decimal ChangeAmount { get; set; }

        /// <summary>
        /// 原因
        /// </summary>
        [Required]
        public string Reason { get; set; }

        /// <summary>
        /// 操作者Id
        /// </summary>
        [Required]
        public Guid OperatorId { get; set; }

        /// <summary>
        /// 金额变动时间
        /// </summary>
        [Required]
        public DateTime EstablishedTime { get; set; }
    }
}
