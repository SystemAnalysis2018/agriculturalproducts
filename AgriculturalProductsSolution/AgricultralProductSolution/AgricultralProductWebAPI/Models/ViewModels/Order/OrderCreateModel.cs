﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AgricultralProductWebAPI.Models.ViewModels.Order
{
    public class OrderCreateModel
    {
        /// <summary>
        /// 买家Id
        /// </summary>
        [Required]
        public Guid BuyerId { get; set; }

        /// <summary>
        /// 产品Id
        /// </summary>
        [Required]
        public Guid ProductId { get; set; }

        /// <summary>
        /// 价格
        /// </summary>
        [Required]
        public int Price { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Required]
        public int Amount { get; set; }

        /// <summary>
        /// 订单状态
        /// </summary>
        [Required]
        public string Status { get; set; }

        /// <summary>
        /// 出单付款时间
        /// </summary>
        [Required]
        public DateTime PayTime { get; set; }

        /// <summary>
        /// 发货时间
        /// </summary>
        [Required]
        public DateTime SendOutTime { get; set; }

        /// <summary>
        /// 确认收货时间
        /// </summary>
        [Required]
        public DateTime ReceiveTime { get; set; }
    }
}
