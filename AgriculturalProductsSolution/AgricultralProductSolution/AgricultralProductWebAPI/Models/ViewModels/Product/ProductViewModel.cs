﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AgricultralProductWebAPI.Models.ViewModels.Product
{
    public class ProductViewModel
    {
        /// <summary>
        /// 商品Id
        /// </summary>
        public Guid Id { get; set; }


        /// <summary>
        /// 卖家Id
        /// </summary>
        public Guid SellerId { get; set; }


        /// <summary>
        /// 商品名称
        /// </summary>
        [Required]
        public string ProductName { get; set; }

       /// <summary>
        /// 是否上架标志
        /// </summary>
        [Required]
        public bool IsForSale { get; set; }

        ///// <summary>
        ///// 图片信息
        ///// </summary>
        //[Required]
        //public byte[] Picture { get; set; }

        /// <summary>
        /// 价格
        /// </summary>
        [Required]
        public decimal Price { get; set; }

       /// <summary>
        /// 商品数量
        /// </summary>
        [Required]
        public int Amount { get; set; }

        /// <summary>
        /// 商品文字信息
        /// </summary>
        [Required]
        public string Characteristic { get; set; }

    }
}
