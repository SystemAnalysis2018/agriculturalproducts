﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AgricultralProductWebAPI.Models.ViewModels.Product
{
    public class ProductModifyAmountModel
    {
        /// <summary>
        /// 商品Id
        /// </summary>
       
        public Guid Id { get; set; }

        /// <summary>
        /// 商品数量增量
        /// </summary>
        [Required]
        [Range(minimum: 1,maximum:10, ErrorMessage = "取值范围1~10")]
        public int AddAmount { get; set; }

    }
}
