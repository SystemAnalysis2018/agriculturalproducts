﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AgricultralProductWebAPI.Models.ViewModels.Seller
{
    public class SellerRegiste
    {
        /// <summary>
        /// 昵称
        /// </summary>
        [Required]
        public string NickName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// 本名 
        /// </summary>
        [Required]
        public string RealName { get; set; }

        /// <summary>
        /// 店铺地址
        /// </summary>
        [Required]
        public string StoreAddress { get; set; }

        /// <summary>
        /// 店铺联系电话
        /// </summary>
        [Required]
        public string StoreTel { get; set; }
    }
}
