﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AgricultralProductWebAPI.Models.ViewModels.ShoppingCart
{
    public class ShoppingCartModifyAmountViewModel
    {
        /// <summary>
        /// 购物车Id
        /// </summary>
        
        public Guid Id { get; set; }


        /// <summary>
        /// 新商品数量
        /// </summary>
        public int NewAmount { get; set; }


    }
}
