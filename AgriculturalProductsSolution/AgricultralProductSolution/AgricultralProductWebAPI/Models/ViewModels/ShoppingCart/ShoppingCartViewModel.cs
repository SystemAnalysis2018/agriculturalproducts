﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AgricultralProductWebAPI.Models.ViewModels.ShoppingCart
{
    public class ShoppingCartViewModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// 买家Id
        /// </summary>
        [Required]
        public Guid BuyerId { get; set; }

        /// <summary>
        /// 商品Id
        /// </summary>
        [Required]
        public Guid ProductId { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        [Required]
        public string ProductName { get; set; }

        /// <summary>
        /// 商品单价
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 商品数量
        /// </summary>
        [Required]
        public int Amount { get; set; }

        /// <summary>
        /// 图片信息
        /// </summary>
        [Required]
        public byte[] Picture { get; set; }


    }
}