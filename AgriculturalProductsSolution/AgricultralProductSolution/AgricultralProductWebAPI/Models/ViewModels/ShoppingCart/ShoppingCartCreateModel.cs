﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AgricultralProductWebAPI.Models.ViewModels.ShoppingCart
{
    public class ShoppingCartCreateModel
    {
        /// <summary>
        /// 买家Id
        /// </summary>
        [Required]
        public Guid BuyerId { get; set; }

        /// <summary>
        /// 商品Id
        /// </summary>
        [Required]
        public Guid ProductId { get; set; }

        /// <summary>
        /// 商品数量
        /// </summary>
        [Required]
        [Range(minimum:1,maximum:10,ErrorMessage = "取值范围为1~10")]
        public int Amount { get; set; }


    }
}
