﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArticlesApi.Models.ViewModels.Login
{
    public class LoginModel
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string Uid { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Pwd { get; set; }

        /// <summary>
        /// Cookie中对应Captcha的值
        /// </summary>
        public string Captcha { get; set; }

        /// <summary>
        /// 答案
        /// </summary>
        public string Answer { get; set; }

        /// <summary>
        /// 登陆类型
        /// </summary>
        public LoginType? LoginType { get; set; }

        
    }

    public enum LoginType
    {
        Seller,
        Buyer,
        Boss
    }
}
