﻿namespace ArticlesApi.Models.Captcha
{
    public class VerifyResponse
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
