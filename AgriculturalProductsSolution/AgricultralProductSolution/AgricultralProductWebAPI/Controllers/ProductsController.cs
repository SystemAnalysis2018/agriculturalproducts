﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AgricultralProductWebAPI.Models.EF;
using AgricultralProductWebAPI.Models.Tools;
using AgricultralProductWebAPI.Models.ViewModels.Product;

namespace AgricultralProductWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly MyContext _context;

        public ProductsController(MyContext context)
        {
            _context = context;
        }

        // GET: api/Products
        [HttpGet]
        public IEnumerable<Product> GetProducts()
        {
            return _context.Products;
        }

        // GET: api/Products
        [HttpGet("AllForSale")]
        public IEnumerable<Product> GetProductsAllForSale()
        {
            return _context.Products.Where(m => m.IsForSale);

        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProduct([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var product = await _context.Products.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        // GET: api/Products/BySellerId/5
        [HttpGet("BySellerId/{id}")]
        public async Task<IActionResult> GetProductsBySellerId([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var seller = await _context.Sellers.FindAsync(id);

            if (seller == null)
            {
                return NotFound();
            }

            var Product = _context.Products.Where(m => m.SellerId == id);

            var list = new List<ProductViewModel>();
            foreach (var product in Product)
            {
                var obj = new ProductViewModel();
                MyMain.CopyObjectData(product, obj, "EstablishedTime,PutOnTime");


                list.Add(obj);
            }
            return Ok(list);
        }

        // PUT: api/Products/5
        [HttpPut("{id}/updown")]
        public async Task<IActionResult> PutProductIsForSale([FromRoute] Guid id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            product.IsForSale = !product.IsForSale;
            await _context.SaveChangesAsync();

            return NoContent();

            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            //if (id != product.Id)
            //{
            //    return BadRequest();
            //}

            //_context.Entry(product).State = EntityState.Modified;

            //try
            //{
            //    await _context.SaveChangesAsync();
            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //    if (!ProductExists(id))
            //    {
            //        return NotFound();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}

            //return NoContent();
        }

        // PUT: api/Products/5
        [HttpPut("{id}/Amount")]
        public async Task<IActionResult> PutProductAmount([FromRoute] Guid id, [FromBody] ProductModifyAmountModel model)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            product.Amount += model.AddAmount;
            if (product.Amount < 0)
            {
                return BadRequest("入库数量不能为负");
            }

            await _context.SaveChangesAsync();

            return NoContent();


        }

        // POST: api/Products
        [HttpPost]
        public async Task<IActionResult> PostProduct([FromBody] ProductCreateModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //if (await _context.Sellers.FindAsync(model.SellerId) == null)
            //{
            //    return BadRequest("该卖家不存在");
            //}

            Product product = new Product()
            {
                Id = Guid.NewGuid(),
                ProductName = model.ProductName,
                SellerId = model.SellerId,
                IsForSale = true,
                Price = model.Price,
                Characteristic = model.Characteristic,
                Amount = model.Amount,
                EstablishedTime = System.DateTime.Now,
                PutOnTime = System.DateTime.Now

            };



            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProduct", new { id = product.Id }, product);
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return Ok(product);
        }

        private bool ProductExists(Guid id)
        {
            return _context.Products.Any(e => e.Id == id);
        }
    }
}