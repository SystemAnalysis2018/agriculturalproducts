﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AgricultralProductWebAPI.Models.EF;
using AgricultralProductWebAPI.Models.Tools;
using AgricultralProductWebAPI.Models.ViewModels.Seller;
using Microsoft.AspNetCore.Authorization;

namespace AgricultralProductWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(Roles = "Boss,Seller,Buyer")]
    public class SellersController : ControllerBase
    {
        private readonly MyContext _context;

        public SellersController(MyContext context)
        {
            _context = context;
        }

        // GET: api/Sellers
        [HttpGet]
        public IEnumerable<Seller> GetSellers()
        {
            return _context.Sellers;
        }

        // GET: api/Sellers/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSeller([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var seller = await _context.Sellers.FindAsync(id);

            if (seller == null)
            {
                return NotFound();
            }

            return Ok(seller);
        }

        // PUT: api/Sellers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSeller([FromRoute] Guid id, [FromBody] Seller seller)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != seller.Id)
            {
                return BadRequest();
            }

            _context.Entry(seller).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SellerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Sellers
        [HttpPost]
        public async Task<IActionResult> PostSeller([FromBody] SellerRegiste model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Seller seller = new Seller()
            {
                Id = Guid.NewGuid(),
                NickName = model.NickName,
                RealName = model.RealName,
                StoreAddress = model.StoreAddress,
                StoreTel = model.StoreTel,
                EstablishedTime = System.DateTime.Now,
                Password = MyMain.Md5Hash(model.Password)

            };
      
            _context.Sellers.Add(seller);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSeller", new { id = seller.Id }, seller);
        }

        // DELETE: api/Sellers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSeller([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var seller = await _context.Sellers.FindAsync(id);
            if (seller == null)
            {
                return NotFound();
            }

            _context.Sellers.Remove(seller);
            await _context.SaveChangesAsync();

            return Ok(seller);
        }

        private bool SellerExists(Guid id)
        {
            return _context.Sellers.Any(e => e.Id == id);
        }
    }
}