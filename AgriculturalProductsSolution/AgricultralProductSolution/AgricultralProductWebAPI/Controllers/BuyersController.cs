﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AgricultralProductWebAPI.Models.EF;
using AgricultralProductWebAPI.Models.Tools;
using AgricultralProductWebAPI.Models.ViewModels.Buyer;
using AgricultralProductWebAPI.Models.ViewModels.ShoppingCart;

namespace AgricultralProductWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BuyersController : ControllerBase
    {
        private readonly MyContext _context;

        public BuyersController(MyContext context)
        {
            _context = context;
        }

        // GET: api/Buyers
        [HttpGet]
        public IEnumerable<Buyer> GetBuyers()
        {
            return _context.Buyers;
        }

        // GET: api/Buyers/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBuyer([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var buyer = await _context.Buyers.FindAsync(id);

            if (buyer == null)
            {
                return NotFound();
            }

            return Ok(buyer);
        }

        // PUT: api/Buyers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBuyer([FromRoute] Guid id, [FromBody] Buyer buyer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != buyer.Id)
            {
                return BadRequest();
            }

            _context.Entry(buyer).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BuyerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Buyers
        [HttpPost]
        public async Task<IActionResult> PostBuyer([FromBody] BuyerRegiste model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Buyer buyer = new Buyer()
            {
                Id = Guid.NewGuid(),
                NickName = model.NickName,
                Address = model.Address,
                Tel = model.Tel,
                EstablishedTime = System.DateTime.Now,
                Password = MyMain.Md5Hash(model.Password)
            };

            _context.Buyers.Add(buyer);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBuyer", new { id = buyer.Id }, buyer);
        }

        // DELETE: api/Buyers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBuyer([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var buyer = await _context.Buyers.FindAsync(id);
            if (buyer == null)
            {
                return NotFound();
            }

            _context.Buyers.Remove(buyer);
            await _context.SaveChangesAsync();

            return Ok(buyer);
        }





        // GET: api/Buyers/5/ShoppingCart
        [HttpGet("{id}/ShoppingCart")]
        public IActionResult GetBuyerShoppingCart([FromRoute] Guid id)
        {
            return RedirectToAction("GetShoppingCartsByBuyerId", "ShoppingCarts", new { id = id });
        }



        private bool BuyerExists(Guid id)
        {
            return _context.Buyers.Any(e => e.Id == id);
        }
    }
}