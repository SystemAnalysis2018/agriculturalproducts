﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;

namespace AgricultralProductWebAPI.Controllers
{
    public class FilesController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public FilesController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        public IActionResult Index()
        {
            return RedirectToAction("download");
        }

        public FileStreamResult download(string id)
        {
            string fileName =$"{id}.png";//客户端保存的文件名
            string filePath = "C:\\Pics\\" + fileName;//路径



            if (!System.IO.File.Exists(filePath))
                filePath = _hostingEnvironment.WebRootPath + "\\images\\Default.png";

            return File(new FileStream(filePath, FileMode.Open), "image/png",fileName);
        }
    }
}