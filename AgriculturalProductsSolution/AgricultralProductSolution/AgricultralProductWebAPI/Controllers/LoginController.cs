﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AgricultralProductWebAPI.Models.EF;
using AgricultralProductWebAPI.Models.Tools;
using ArticlesApi.Models.Captcha;
using ArticlesApi.Models.ViewModels.Login;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace ArticlesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {


        private readonly IConfiguration _configuration;
        private readonly MyContext _context;

        /// <summary>
        /// 依赖注入
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="context"></param>
        public LoginController(IConfiguration configuration, MyContext context)
        {
            _context = context;
            _configuration = configuration;
        }



        [HttpPost]
        public async Task<IActionResult> Post([FromBody] LoginModel model)
        {
            if (model.LoginType == null)
            {
                return BadRequest("错误的登陆类型");
            }
            var model2 = new VerifyRequest()
            {
                Answer = model.Answer,
                Captcha = model.Captcha
            };
            var response = await CaptchaFactory.Intance.VerifyAsync(model2);

            if (response.Code != 100)
                return BadRequest(response.Message);
            var claims = new List<Claim>();
            Guid myid = Guid.Empty;
            switch (model.LoginType)
            {
                case LoginType.Boss:
                    if ((model.Uid != _configuration["BossUserUid"] || model.Pwd != _configuration["BossUserPwd"]))
                        return BadRequest("用户名密码错误");
                    claims.Add(new Claim(ClaimTypes.Role, "Boss"));
                    break;
                case LoginType.Buyer:
                    var buyer = _context.Buyers.FirstOrDefault(m => m.NickName == model.Uid && m.Password == MyMain.Md5Hash(model.Pwd));
                    if (buyer == null)
                        return BadRequest("用户名密码错误");
                    myid = buyer.Id;
                    claims.Add(new Claim(ClaimTypes.Role, "Buyer"));
                    break;
                case LoginType.Seller:
                    var seller =
                        _context.Sellers.FirstOrDefault(m => m.NickName == model.Uid && m.Password == MyMain.Md5Hash(model.Pwd));
                    if (seller == null)
                        return BadRequest("用户名密码错误");
                    myid = seller.Id;
                    claims.Add(new Claim(ClaimTypes.Role, "Seller"));
                    break;
            }


            //sign the token using a secret key.This secret will be shared between your API and anything that needs to check that the token is legit.
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["SecurityKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: "jwttest",
                audience: "jwttest",
                claims: claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: creds);

            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                id = myid
            });
        }




        ///// <summary>
        ///// 返回验证码图片
        ///// </summary>
        ///// <returns></returns>
        //[HttpGet("Captcha")]
        //public async Task<ActionResult> GetCaptcha()
        //{
        //    var model = await CaptchaFactory.Intance.CreateAsync();
        //    Response.Cookies.Append("Captcha", model.Answer);
        //    return File(model.Image, model.ContentType);
        //}

        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <returns></returns>
        [HttpGet("captcha")]
        public async Task<CaptchaInfo> GetCaptcha2()
        {
            var model = await CaptchaFactory.Intance.CreateAsync();
            return model;
        }

        ///// <summary>
        ///// 验证码的校验
        ///// </summary>
        ///// <param name="model"></param>
        ///// <returns></returns>
        //[HttpPost("captcha/verify")]
        //public async Task<IActionResult> Verify([FromBody] LoginModel model)
        //{
        //    var model2 = new VerifyRequest()
        //    {
        //        Answer = model.Answer,
        //        Captcha = model.Captcha
        //    };
        //    var response = await CaptchaFactory.Intance.VerifyAsync(model2);

        //    if (response.Code == 100)
        //        return Ok("验证成功");
        //    return BadRequest(response.Message);
        //}
    }
}
