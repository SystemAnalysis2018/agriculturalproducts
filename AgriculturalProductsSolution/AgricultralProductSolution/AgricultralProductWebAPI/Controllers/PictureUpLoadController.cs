﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace AgricultralProductWebAPI.Controllers
{
    public class PictureUpLoadController : Controller
    {
        //public IActionResult Index()
        //{
        //    return View();
        //}
        public class PicData
        {
            public string Msg { get; set; }
        }

        [HttpPost]
        public async Task<bool> InsertPicture(string id)
        {
            var data = new PicData();
            string path = string.Empty;
            var files = Request.Form.Files;
         
            if (files == null || files.Count() <= 0)
            {
                data.Msg = "请选择上传的文件。";
                return false;
            }

            string fileName = $"{id}.png";//客户端保存的文件名
            string filePath = "C:\\Pics\\" + fileName;//路径


            using (var stream = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                await files[0].CopyToAsync(stream);
            }

            return true;
        }

    }
}