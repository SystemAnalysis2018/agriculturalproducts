﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AgricultralProductWebAPI.Models.EF;
using AgricultralProductWebAPI.Models.ViewModels.ShoppingCart;
using AgricultralProductWebAPI.Models.Tools;

namespace AgricultralProductWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShoppingCartsController : ControllerBase
    {
        private readonly MyContext _context;

        public ShoppingCartsController(MyContext context)
        {
            _context = context;
        }

        // GET: api/ShoppingCarts
        [HttpGet]
        public IEnumerable<ShoppingCart> GetShoppingCarts()
        {
            return _context.ShoppingCarts;
        }

        // GET: api/ShoppingCarts/ByBuyerId/5
        [HttpGet("ByBuyerId/{id}")]
        public async Task<IActionResult> GetShoppingCartsByBuyerId([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var buyer = await _context.Buyers.FindAsync(id);

            if (buyer == null)
            {
                return NotFound();
            }

            var ShoppingCart = _context.ShoppingCarts.Where(m => m.BuyerId == id);


            var list = new List<ShoppingCartViewModel>();
            foreach (var sc in ShoppingCart)
            {
                var obj = new ShoppingCartViewModel();
                MyMain.CopyObjectData(sc, obj, "");
                var product = await _context.Products.FindAsync(sc.ProductId);
                if (product != null)
                {
                    obj.ProductName = product.ProductName;
                    obj.Price = product.Price;
                }

                list.Add(obj);
            }

            return Ok(list);
        }

        //public async Task<IActionResult> GetShoppingCart([FromRoute] Guid id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var shoppingCart = await _context.ShoppingCarts.FindAsync(id);

        //    if (shoppingCart == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(shoppingCart);
        //}


        // PUT: api/ShoppingCarts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutShoppingCart([FromRoute] Guid id, [FromBody] ShoppingCart shoppingCart)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != shoppingCart.Id)
            {
                return BadRequest();
            }

            _context.Entry(shoppingCart).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShoppingCartExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ShoppingCarts
        [HttpPost]
        public async Task<IActionResult> PostShoppingCart([FromBody] ShoppingCartCreateModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //if (await _context.Buyers.FindAsync(model.BuyerId) == null)
            //{
            //    return BadRequest("该买家不存在");
            //}

            //if (await _context.Products.FindAsync(model.ProductId) == null)
            //{
            //    return BadRequest("该商品不存在");
            //}

            var oldItem =
                await _context.ShoppingCarts.FirstOrDefaultAsync(m =>
                    m.BuyerId == model.BuyerId && m.ProductId == model.ProductId);
            if (oldItem == null)
            {
                ShoppingCart shoppingCart = new ShoppingCart()
                {
                    Id = Guid.NewGuid(),
                    BuyerId = model.BuyerId,
                    ProductId = model.ProductId,
                    Amount = model.Amount,
                    AddInTime = System.DateTime.Now
                };
                

                _context.ShoppingCarts.Add(shoppingCart);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetShoppingCart", new { id = shoppingCart.Id }, shoppingCart);
            }
            else
            {
                oldItem.Amount += model.Amount;
                oldItem.AddInTime = System.DateTime.Now;
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetShoppingCart", new { id = oldItem.Id }, oldItem);

            }
        }

        // DELETE: api/ShoppingCarts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteShoppingCart([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var shoppingCart = await _context.ShoppingCarts.FindAsync(id);
            if (shoppingCart == null)
            {
                return NotFound();
            }

            _context.ShoppingCarts.Remove(shoppingCart);
            await _context.SaveChangesAsync();

            return Ok(shoppingCart);
        }


        //[HttpGet("/ByBuyerId/{id}")]
        //public IActionResult GetShoppingCartByBuyerId([FromRoute] Guid id)
        //{
        //    return RedirectToAction("GetBuyerShoppingCart", "Buyers", new { id = id });
        //}


        private bool ShoppingCartExists(Guid id)
        {
            return _context.ShoppingCarts.Any(e => e.Id == id);
        }
    }
}